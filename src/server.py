#!/usr/bin/env python
import os
import sys
import urlparse
from importlib import import_module

import tornado
from tornado.options import define, options
from tornado.httpclient import HTTPRequest, AsyncHTTPClient
from tornado.web import Application, RequestHandler
from tornado.ioloop import IOLoop
from django.apps import apps
from django.conf import settings

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'settings')
apps.populate(settings.INSTALLED_APPS)

from django.contrib import auth

session_engine = import_module(settings.SESSION_ENGINE)

define('addr', default='127.0.0.1', help='Address to listen on')
define('port', default=8000, help='Port to listen on')
define('upstream_url', default='http://localhost', help='URI of upstream server')

AsyncHTTPClient.configure('tornado.curl_httpclient.CurlAsyncHTTPClient')


COMMON_PROTOCOL_PORTS = {
    'http': 80,
    'https': 443
    }


def has_django_authentication(func):
    def decorator(obj, *args, **kw):
        if obj.get_current_user():
            return func(obj, *args, **kw)
        raise tornado.web.HTTPError(401)
    return decorator


class MockDjangoRequest(object):
    def __init__(self, session):
        self.session = session


class ProxyAuthenticationHandler(RequestHandler):
    SUPPORTED_METHODS = ('GET', 'POST', 'PUT', 'DELETE', 'HEAD', 'OPTIONS')

    def initialize(self, upstream_url):
        self.future = None
        self._client = AsyncHTTPClient()
        self._upstream = urlparse.urlparse(upstream_url)
        self._port = self._upstream.port or COMMON_PROTOCOL_PORTS.get(self._upstream.scheme)
        if self._port is None:
            raise ValueError('Bad configuration for upstream URL. Missing port')

    @has_django_authentication
    @tornado.web.asynchronous
    @tornado.gen.engine
    def forward(self):
        response = yield tornado.gen.Task(self._client.fetch, self.rewrite(self.request))
        for name, value in response.headers.items():
            self.set_header(name, value)
        self.write(response.body)
        self.finish()

    def get_current_user(self):
        try:
            session_store = session_engine.SessionStore
            session_cookie = self.request.cookies.get(settings.SESSION_COOKIE_NAME, None)
            session = session_store(session_cookie.value)
            return auth.get_user(MockDjangoRequest(session))
        except AttributeError:
            return None

    def rewrite(self, request):
        url = '%s://%s:%s%s' % (
            self._upstream.scheme,
            self._upstream.hostname,
            self._port,
            request.path
            )
        return HTTPRequest(
            url=url,
            method=self.request.method,
            headers=self.request.headers,
            body=self.request.body or None
        )

    get = post = put = delete = head = options = forward


def run():
    try:
        app_server = Application([
            (r'.*', ProxyAuthenticationHandler, {'upstream_url': options.upstream_url})
        ], debug=settings.DEBUG)
        app_server.listen(options.port, options.addr)
        IOLoop.instance().start()
    except KeyboardInterrupt:
        sys.exit()

if __name__ == '__main__':
    options.parse_command_line()
    run()
