#!/usr/bin/env python

from setuptools import setup, find_packages

setup(
    name='django-tornado-auth-gateway',
    url='https://bitbucket.org/lullis/django-tornado-auth-gateway',
    version='0.1.0',
    packages=find_packages('src'),
    package_dir={'': 'src'},
    install_requires=[
        'django',
        'tornado',
        'pycurl',
        'certifi'
        ],
    zip_safe=False,
    classifiers=[
        'Operating System :: Linux'
    ],
    keywords='web server proxy authentication django tornado'
)
